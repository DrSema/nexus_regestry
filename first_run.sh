#!/bin/bash
# Set env from .env file
source ./.env

# pkg registry Nexus: NPM, NUGET
mkdir -p $ROOT_DATA_DIR/nexus-data
chown -R 200:200 $ROOT_DATA_DIR/nexus-data

# Copy nginx configs
mkdir -p $ROOT_DATA_DIR/nginx
sudo cp -r ./nginx $ROOT_DATA_DIR/

# Generate self-signed cert
mkdir -p $HTTPS_CERT_DIR
#./scripts/create_certificate_for_domain.sh wwww.mydomain.com $HTTPS_CERT_DIR
