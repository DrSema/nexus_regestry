if [ -z "$1" ]
then
  echo "Need domainname";
  echo "e.g. mysite.com"
  exit;
fi

if [ -z "$2" ]
then
  echo "Need out cert dir";
  echo "e.g. /data/nginx/conf/ssl"
  exit;
fi

openssl req -x509 -newkey rsa:4096 -keyout $2/$1_key.pem -out $2/$1_cert.pem -nodes -days 365 -subj '/CN='$1
